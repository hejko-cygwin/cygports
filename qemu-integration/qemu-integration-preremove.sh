#/bin/bash

BAK=$(date -Idate)
mv /etc/qemu-integration.conf "/etc/qemu-integration.conf.$BAK"
export DISABLE_INTEGRATION=true
qemu-integration-update.sh
mv "/etc/qemu-integration.conf.$BAK" /etc/qemu-integration.conf
