Cygports for Cygwin software packages
=====================================

Cygport files describe how to build Cygwin packages. See [Cygport documentation][1] for details.

This repository contains mostly cygport files for software not/outdated in Cygwin:

* QEMU - the FAST! processor emulator [QEMU][2] is a generic and open source machine emulator and virtualizer.
* SPICE - Simple Protocol for Independent Computing Environments [SPICE][4] is an open source solution for remote access to virtual machines in a seamless way
* Glib - Low-level core library that forms the basis for projects such as GTK+ and GNOME [Glib][5]. This glib2.0 is a patched 2.58.3 for better SPICE support which fulfilles current QEMU requirements as well.
* libslirp - user-mode networking library used by virtual machines, containers or various tools. starting with QEMU version 7.2.0, [libslirp][6] is no longer provided by QEMU
* Cygwin Setup helper empty-local-cygwin-cache for removing the local cygwin cache directly after package installation, which is obsolete if a local proxy mirror like [Apt-Cacher NG][3] exists.

The helper script cygport.sh is provided for easy and compatible integration into Cygwin.

Qemu installation
-----------------

Installing Qemu requires temporarily 10GB HD and takes quiet some time (approximately 2h on my G3240 CPU).

To install Qemu to Cygwin and clean up afterwards, perform:

```
./cygport.sh mingw64-x86_64-libslirp cygwinInstall clean
./cygport.sh mingw64-x86_64-qemu cygwinInstall clean
```

Finally update qemu-integration calling `qemu-integration-update.sh` to make Qemu accessible in Cygwin shell.

### Alternative: Qemu with SPICE

To add SPICE support to Qemu, perform

```
./cygport.sh mingw64-x86_64-glib2.0 cygwinInstall clean
./cygport.sh mingw64-x86_64-libslirp cygwinInstall clean
./cygport.sh mingw64-x86_64-spice-protocol cygwinInstall clean
./cygport.sh mingw64-x86_64-spice cygwinInstall clean
./cygport.sh mingw64-x86_64-qemu cygwinInstall clean
```

Using the helper script cygport.sh
----------------------------------

`./cygport.sh <NAME> cygwinInstall [CYGPORTOPTION]`
* downloads sources, builds the Cygwin package and installs to Cygwin

`./cygport.sh <NAME> clean`
* removes everything in build dir but the downloaded sources and the previously built Cygwin package

`./cygport.sh <NAME> distclean`
* removes every previously created build dir

`./cygport.sh <NAME> cygwinUninstall`
* uninstalls the Cygwin package from Cygwin

`./cygport.sh <NAME> <CYGPORTCOMMAND> [CYGPORTOPTION]`
* calls `/usr/bin/cygport [CYGPORTOPTION] <NAME> <CYGPORTCOMMAND>`

Multiple commands are processed in given order.

[1]: <https://cygwin.github.io/cygport> (Cygport documentation)
[2]: <https://www.qemu.org> (QEMU)
[3]: <https://www.unix-ag.uni-kl.de/~bloch/acng/> (Apt-Cacher NG)
[4]: <https://www.spice-space.org> (SPICE)
[5]: <https://gitlab.gnome.org/GNOME/glib> (Glib)
[6]: <https://gitlab.freedesktop.org/slirp/libslirp> (libslirp)
