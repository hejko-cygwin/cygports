#!/bin/bash

LAST_MIRROR_DIR=$( echo $(grep -A1 "^last-mirror\s*$" /etc/setup/setup.rc | grep -v "last-mirror" | sed "s|/|%2f|g" | sed "s|:|%3a|g" ) )
if [[ $LAST_MIRROR_DIR =~ (http|ftp) ]] && [[ $LAST_MIRROR_DIR =~ cygwin ]] && [ -d "/$LAST_MIRROR_DIR" ]
then
	echo "Trying to empty /$LAST_MIRROR_DIR/noarch/release"
	rm -rf "/$LAST_MIRROR_DIR/noarch/release"
	echo "Trying to empty /$LAST_MIRROR_DIR/x86/release"
	rm -rf "/$LAST_MIRROR_DIR/x86/release"
	echo "Trying to empty /$LAST_MIRROR_DIR/x86_64/release"
	rm -rf "/$LAST_MIRROR_DIR/x86_64/release"
else
	echo "/$LAST_MIRROR_DIR/ not emptyable"
fi

