#!/bin/bash

function cygportName {
	echo "$CYGPORTNAME" | sed "s%/*$%%"
}

function cygportDir {
	echo "$(dirname "$(realpath "$0")")/$(cygportName)"
}

function cygportCall {
	local CYGPORTCOMMAND=$1
	(
	cd "$(cygportDir)"
	echo cygport $CYGPORTOPTIONS $(cygportName).cygport $CYGPORTCOMMAND
	cygport $CYGPORTOPTIONS "$(cygportName).cygport" "$CYGPORTCOMMAND"
	)
}

function inherit {
	true
}

function cygportEnv {
	local CYGPORTNAME=$(cygportName)
	source "$(cygportDir)/$CYGPORTNAME.cygport"
	if [ -z "$ARCH" ]
	then
		[[ $CYGPORTNAME =~ ^mingw ]] && ARCH=noarch || ARCH=$(uname -m)
	fi
	[ -n "$NAME" ] && [ -n "$VERSION" ] && [ -n "$RELEASE" ]
}

function distclean {
	(
	if cygportEnv
	then
		echo
		cd "$(cygportDir)"
		for DIR in $(ls -d $NAME-*.$ARCH)
		do
			if [ -d "$DIR" ]
			then
				echo "Removing $DIR ..."
				rm -rf "$DIR"
			fi
		done
		echo "Finished."
		echo
	fi
	)
}

function clean {
	(
	if cygportEnv
	then
		echo
		DIR="$(cygportDir)/$NAME-$VERSION-$RELEASE.$ARCH"
		if [ -d "$DIR" ]
		then
			echo "Cleaning $DIR ..."
			cd "$DIR"
			for F in $(ls)
			do
				if [ "dist" != "$F" ]
				then
					rm -rf "$F"
				fi
			done
			SRC_PREFIX="$NAME-$VERSION-$RELEASE-src"
			find dist -name "$SRC_PREFIX*" -exec rm -f {} \;
		fi
		echo "Finished."
		echo
	fi
	)
}

function installedDb {
	echo "/etc/setup/installed.db"
}

function allDepsInstalled {
	local DB="$(installedDb)"
	local DEPENDENCY
	for DEPENDENCY in $@
	do
		grep -q "^$DEPENDENCY " $DB || return 1
	done
}

function cygportTar {
	cygportEnv
	echo "$(cygportDir)/$NAME-$VERSION-$RELEASE.$ARCH/dist/$NAME/$NAME-$VERSION-$RELEASE.tar.xz"
}

function hasCygportTar {
	if [ -n "$HAS_TAR" ]
	then
		# HAS_TAR is set to y only if an tar already exists
		[ "$HAS_TAR" == "y" ] && return 0 || return 1
	fi

	local TAR="$(cygportTar)"
	if [ -f "$TAR" ]
	then
		if [ "$DEPS_TYPE" == "install" ]
		then
			read -p "Install already existing package? ([y]|n) " HAS_TAR
		fi
		if [ "$HAS_TAR" != "n" ]
		then
			HAS_TAR="y"
		fi
	else
		HAS_TAR="n"
	fi
	hasCygportTar
}

function showDependencies {
	# No deps, if DEPS_TYPE not set
	[ -z "$DEPS_TYPE" ] && return
	echo
	cygportEnv
	if hasCygportTar && [ "$DEPS_TYPE" == "install" ]
	then
		local DEPENDENCIES="$REQUIRES"
	else
		DEPS_TYPE="build"
		local DEPENDENCIES="cygport $REQUIRES $BUILD_REQUIRES"
	fi
	if allDepsInstalled $DEPENDENCIES
	then
		echo "All dependencies for $DEPS_TYPE are already installed."
	else
		local SETUP="setup-$([ "$(uname -m)" == "x86_64" ] && echo "x86_64" || echo "x86").exe"
		echo "Make sure the dependencies for build and execution are installed!"
		echo "Execute the command line in Cygwin Terminal after customizing <SETUPPATH>:"
		echo
		echo "<SETUPPATH>/$SETUP -P \\"
		echo $DEPENDENCIES | sed "s/\s\s*$//" | fold -s - | sed "s/^\s*//" | sed "s/\s\s*$/,\\\\/" | sed "s/\s\s*/,/g"
		echo

		local TEST=""
		read -p "Are all dependencies installed? (y|[n]) " TEST
		[ "y" == "$TEST" ] || exit
	fi
	echo
}

function backupInstalledDb {
	local DB="$(installedDb)"
	echo
	echo "Creating Backup of $DB..."
	if cp -pv $DB "$DB.$(date +%s).bak"
	then
		sleep 1
	else
		echo "Failure, won't touch DB."
		echo
		return 1
	fi
}

function removeFromInstalledDb {
	backupInstalledDb || return 1
	local DB="$(installedDb)"
	cygportEnv
	echo "Remove $NAME from $DB"
	echo
	grep -v "^$NAME " $DB > $DB.del
	mv $DB.del $DB
}

function addToInstalledDb {
	backupInstalledDb || return 1
	local DB="$(installedDb)"
	cygportEnv
	echo "Add $NAME to $DB"
	echo
	cp $DB $DB.add
	echo "$NAME $NAME-$VERSION-$RELEASE.tar.bz2 1" >> $DB.add
	sort $DB.add > $DB
	rm $DB.add
}

function setupList {
	cygportEnv
	echo "/etc/setup/$NAME.lst.gz"
}

function catSetupList {
	zcat "$(setupList)"
}

function cygwinInstall {
	local SETUPLIST="$(setupList)"
	if [ -f "$SETUPLIST" ]
	then
		# Uninstall before reinstall
		local REINSTALL=1
		cygwinUninstall
	fi
	echo "----------------------------"
	echo "Installing $(cygportName)..."
	echo "----------------------------"
	local TAR="$(cygportTar)"
	if [ -f "$TAR" ]
	then
		addToInstalledDb
		tar -tf "$TAR" | gzip -c > $SETUPLIST
		tar -C / -xvf "$TAR"
		catSetupList | grep "etc/postinstall" | while read LINE
		do
			[ -x "/$LINE" ] && [ ! -d "/$LINE" ] && "/$LINE"
		done
		return 0
	fi
	return 1
}

function cygwinUninstall {
	local SETUPLIST="$(setupList)"
	if [ "$REINSTALL" != "1" ]
	then
		echo
		echo "Executing cygwinUninstall may break things."
		echo "Make sure no other package depends on this package."
		read -p "Really go on? (CTRL-C to stop) " TEST
		echo
	fi
	echo "------------------------------"
	echo "Uninstalling $(cygportName)..."
	echo "------------------------------"
	echo
	if [ ! -f "$SETUPLIST" ]
	then
		echo "$(cygportName) not installed!"
		echo
		return 1
	fi
	catSetupList | grep "etc/preremove" | while read LINE
	do
		[ -x "/$LINE" ] && [ ! -d "/$LINE" ] && "/$LINE"
	done
	catSetupList | sort -r | while read LINE
	do
		if [ -d "/$LINE" ]
		then
			rmdir --ignore-fail-on-non-empty "/$LINE"
		elif [ -f "/$LINE" ]
		then
			rm -fv "/$LINE"
		fi
	done
	rm -v $SETUPLIST
	removeFromInstalledDb
}

function executeCommands {
	local CYGPORTNAME=$1
	if [ -z "$(cygportName)" ] || [ ! -d "$(cygportDir)" ]
	then
		echo "Cygport '$(cygportName)' is invalid"
		exit 1
	fi
	shift
	local CYGPORTOPTIONS=""
	local PARAM DEPS_TYPE
	for PARAM in "$@"
	do
		case $PARAM in
			-*)
				# Collect cygport options
				CYGPORTOPTIONS="$CYGPORTOPTIONS $PARAM"
				;;
			distclean)
				HAS_TAR="n"
				;;
			clean|cygwinUninstall)
				# Ignore commands here
				;;
			cygwinInstall)
				[ -z "$DEPS_TYPE" ] && DEPS_TYPE="install"
				;;
			*)
				# Any cygport command
				DEPS_TYPE="build"
				HAS_TAR="n"
				;;
		esac
	done
	showDependencies
	local COMMAND RET
	for COMMAND in "$@"
	do
		case $COMMAND in
			distclean)
				distclean
				;;
			clean)
				clean
				;;
			cygwinInstall)
					( hasCygportTar || ( cygportCall download && cygportCall all ) ) &&
					cygwinInstall
				;;
			cygwinUninstall)
				cygwinUninstall
				;;
			-*)
				# Ignore cygport options
				;;
			*)
				cygportCall $COMMAND
				;;
		esac
		RET="$?"
		[ "$RET" == "0" ] || exit $RET
	done
}

executeCommands "$@"
